# Reverse a list

list4 = [100, 200, 300, 400, 500]
list4.reverse()
print(list4)

# Concatenate two lists by index
list1 = ["M", "na", " ", " "]
list2 = ["y", "me", "is", "Anaka"]
list3 = []

for i in range(len(list1)):
    list3.append(list1[i] + list2[i])
result = str(list3)

print(result)

# Turn every item of a list into its square
numbers = [1, 2, 3, 4, 5, 6, 7]

for num in numbers:
    num = num**2
    print(num)


# Given two lists, iterate both lists simultaneously and display items from
# list1 in the original order and items from list2 in reverse order.
list5 = [10, 20, 30, 40]
list6 = [100, 200, 300, 400]
list6.reverse()

list7 = [list(i) for i in zip(list5, list6)]
print(list7)

# Remove empty strings from the list
list8 = ["Mike", "", "Emma", "Aisha", "", "Anaka"]
filtered_list = [i for i in list8 if i != ""]
print(filtered_list)


# OR
def check_list(name):
    if name != "":
        return True


filtered_list = filter(check_list, list8)
result = list(filtered_list)
print(result)

# Add new item to a list after a specified item
# Task: Add item 7000 after 6000 in the following list
list9 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
list9[2][2].append(7000)
print(list9)

# Extend nested list by adding the sub-list
# Expected output ['a', 'b', ['c', ['d', 'e', ['f', 'g', 'h', 'i', 'j'],
#  'k'], 'l'], 'm', 'n']
list10 = ["a", "b", ["c", ["d", "e", ["f", "g"], "k"], "l"], "m", "n"]
sub_list = ["h", "i", "j"]
# print(list10[2]) print to find the index if can't figure out
list10[2][1][2].extend(sub_list)
print(list10)

# Keys, values, dictionaries
numbers_to_words = {
    1: "one",
    2: "two",
    3: "three",
}

for key in numbers_to_words:
    print("The key is key:", key)

    # Use the key to get the value from
    # the dictionary
    value = numbers_to_words[key]
    print("The associated value is:", value)


countries = {
    "Asia": ["China", "Mongolia", "India"],
    "South America": ["Brazil", "Argentina", "Chile"],
    "North America": ["United States", "Canada", "Mexico"],
    "Antarctica": [],
    "Africa": ["South Africa", "Algeria", "Kenya", "Ethiopia", "Egypt"],
    "Europe": ["France", "Germany", "England", "Spain", "Greece", "Italy"],
    "Australia": ["New Zealand", "Fiji", "Australia"],
}

# Use the dictionary to print the value of "Asia"

value = countries["Asia"]
print(value)


# use the dictionary to print the third item in the value of "Australia"
values = countries["Australia"]
print(values[2])

# print a list of all the keys, in alphabetical order
print(sorted(countries))


# Use the dictionary keys and string interpolation to print out this string:
# "The candidate's name is W.L. McCrea. She has
# 4 years of experience as a graphic designer
# at Voltage Ad in Louisville, Kentucky"

employees = {
    "first_name": "W.L.",
    "last_name": "McCrea",
    "company": "Voltage Ad",
    "company_location": "Louisville, Kentucky",
    "years_experience": 4,
    "role": "graphic designer",
}


print(
    f"The candidate's name is {employees['first_name']} {employees['last_name']}. She has {employees['years_experience']} years of experience as a {employees['role']} at {employees['company']} in {employees['company_location']}"
)

# Print a dictionary that indicates the number of times each letter occurs in the list.

# sort the list alphabetically then count the number of times each letter occurs and add it to a dictionary

letter_list = [
    "s",
    "a",
    "j",
    "q",
    "l",
    "m",
    "v",
    "l",
    "t",
    "h",
    "b",
    "w",
    "r",
    "g",
    "n",
    "c",
    "y",
    "i",
    "z",
    "a",
    "l",
    "t",
    "x",
    "e",
    "k",
    "o",
    "r",
    "u",
    "p",
    "l",
    "n",
    "c",
    "d",
    "q",
    "l",
    "w",
]
my_dict = {}

# check to see if already in dictionary then count the number of times each letter occurs during each iteration
for letter in letter_list:
    if letter not in my_dict:
        my_dict[letter] = 1
    else:
        my_dict[letter] += 1

print(my_dict)


# Loop through the dictionary and create a new one where the values are the squares of the current values.

dict2 = {"a": 1, "b": 2, "c": 3}
new_dict = {}
for key, value in dict2.items():
    new_dict[key] = value**2

# below is if you don't have to create a new dictionary
# dict2.update((key, value**2) for key, value in dict2.items())

print(new_dict)


products = [
    {"item": "Bubble tea", "quantity": 6, "price_per_unit": 5.00},
    {"item": "Unicorn onesie", "price_per_unit": 49.99},
    {"item": "Legal briefcase", "quantity": 1, "price_per_unit": 149.99},
    {
        "item": "Fake mustaches",
        "quantity": 10,
        "price_per_unit": 1.00,
    },
]

# Write some code that will print a list of each of the products
list_of_prods = []
for product in products:
    list_of_prods.append(product["item"])
print(list_of_prods)

sub_total = 0
for product in products:
    if "quantity" in product:
        line_item = product["quantity"] * product["price_per_unit"]
    else:
        line_item = 0.0

    sub_total += line_item

    print(product["item"], line_item)

# write some code that calculates the total invoice + tax of 8%
total = sub_total * 1.08
print(total)


# dict2.update((key, value**2) for key, value in dict2.items())


# If the shipment list is empty, then the function should return 0.
# sum all the weights in the shipment dict
def find_shipment_weight(shipment):
    weight = 0
    for item in shipment:
        weight += item["product_weight_pounds"] * item["quantity"]
    return weight

    # return longest string
def find_longest(strings):
    longest = ""

    if len(strings) == 0:
        return None
    else:
        for string in strings:
            if len(string) > len(longest):
                longest = string
        return longest


# return true if base and number are ints and also
# if number is a multiple of base (6, 2)
def is_multiple_of(number, base):
    if int(number) != number or int(base) != base:
        return False
    return number % base == 0


def shift_cipher(message, shift):
    res = ""
    for char in message:
        char_num = ord(char)
        res += chr(char_num + shift)
    return res


print(shift_cipher("Raining-Frogs", -10))  # expected "HW_d_d]#<he]i"

# return a list of values in the string with delimiter/separator
# removed


def read_delimited(line, separator):
    list = line.split(separator)
    return list


# input = [1,2,3,4,5,6,7]  outcome = [[1,2], [3,4], [5,6]]
def pair_up(items):
    pairs = []
    for i in range(0, len(items), 2):
        if i + 1 < len(items):
            pair = [items[i], items[i + 1]]
            list.append(pair)
    return pairs


# strings, separated by the separator. (["aaa", "bbb", "ccc"], "--") returns
# "aaa--bbb--ccc"
def join_strings(strings, separator):
    str_len = len(strings)
    if str_len == 0:
        return ""

    return separator.join(strings)


list22 = ["aaa", "bbb", "ccc"]
sep = "--"
print(join_strings(list22, sep))


def mean(numbers22):
    if (len(numbers22)) == 0:
        return float("nan")
    totals = sum(numbers)

    return totals / len(numbers)


def get_list_of_managers(company):
    manager_list = []
    for person in company:
        if person.get("position") == "manager":
            manager_list.append(person.get("name"))
    return manager_list


def get_list_of_workers(company):
    worker_list = []
    for person in company:
        if person.get("position") == "worker":
            worker_list.append(person["name"])
    return worker_list


def longest_name(company):
    long_name = ""
    for employee in company:
        if len(employee.get("name")) > len(long_name):
            long_name = employee["name"]
    return long_name


def get_manager_pay(company):
    total = 0
    for person in company:
        if person.get("position") == "manager":
            total += person.get("rate") * person.get("hours")
    return total


def who_made_the_most(company):
    made_most = ""
    most_money = 0
    for employee in company:
        money = employee.get("rate") * employee.get("hours")
        if money > most_money:
            made_most = employee.get("name")
            most_money = money
    return made_most


company1 = [
    {"name": "leah", "position": "manager", "rate": 55, "hours": 15},
    {"name": "freddy", "position": "manager", "rate": 25, "hours": 30},
    {"name": "isaac", "rate": 20, "hours": 20},
    {"name": "murphey", "position": "worker", "rate": 41, "hours": 25},
    {"name": "phil", "position": "worker", "rate": 9, "hours": 45},
]

print(who_made_the_most(company1))


def letter_occurences(str):
    result = ""
    cache = {}
    for letter in str:
        if letter != " ":
            if letter not in cache:
                cache[letter] = 0
            cache[letter] += 1
    for key in cache:
        result += f"There are {cache[key]} {key}'s. "
    return result


nums = [2, 4, 6, 8, 10, 12, 3, 5, 6, 6, 7, 8, 9, 7, 5, 5, 89, 8, 6, 78, 9, 87, 5]


def remove_duplicates(lst):
    cache = {}
    result = []
    for item in lst:
        if item not in cache:
            cache[item] = True
            result.append(item)
    return result


print(remove_duplicates(nums))


# Pretty Printing
# to make your dictionaries and nested lists easy to read, you can add this to the top of your code.


def using_if_variable_in_dictionary(my_dict4):
    result4 = []
    for key in my_dict4.keys():
        if key in my_dict4:
            result4.append(key)
    return result4


my_dict4 = {"a": 1, "b": 2, "c": 3}
print(using_if_variable_in_dictionary(my_dict4))

example = {
    "a": 0,
    "b": 4,
    "c": "x",
    "d": "",
    "e": True,
    "f": False,
    "g": [1, 2, 3],
    "h": [],
    "i": {"a_key": "a value"},
    "j": {},
    "k": None,
}
print(using_if_variable_in_dictionary(example))

# OUTPUT ==> ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']


students = [
    "Stan",
    "Kyle",
    "Cartman",
    "Kenny",
    "Butters",
    "Craig",
    "Clyde",
    "Tweek",
    "Tolken",
    "Wendy",
    "Bebe",
    "Jimmy",
    "Timmy",
]

sent_attendance_token = [
    "Stan",
    "Kyle",
    "Butters",
    "Craig",
    "Tweek",
    "Tolken",
    "Wendy",
    "Bebe",
    "Jimmy",
    "Timmy",
]

did_exploration = [
    "Butters",
    "Craig",
    "Kyle",
    "Tweek",
    "Tolken",
    "Wendy",
    "Bebe",
    "Jimmy",
    "Timmy",
]

finished_project = [
    "Stan",
    "Cartman",
    "Butters",
    "Craig",
    "Tweek",
    "Tolken",
    "Wendy",
    "Jimmy",
]

failed_assessment = ["Cartman", "Kenny", "Clyde", "Butters"]


def who_is_absent(students, sent_tokens):
    cache = {}
    absent = []
    for student in students:
        cache[student] = True
    for token in sent_tokens:
        cache[token] = False
    for kid in cache:
        if cache.get(kid):
            absent.append(kid)
    return absent


print(who_is_absent(students, sent_attendance_token))

# OUTPUT ==>  ['Cartman', 'Kenny', 'Clyde']


def who_skipped_the_exploration(students, exploration):
    cache = {}
    did_not_do_exploration = []
    for name in exploration:
        cache[name] = True
    for student in students:
        if not cache.get(student):
            did_not_do_exploration.append(student)
    return did_not_do_exploration


print(who_skipped_the_exploration(students, did_exploration))

# OUTPUT ==>  ['Stan', 'Cartman', 'Kenny', 'Clyde']


def who_finished_project_and_passed_assessment(projects, assessments):
    cache = {}
    passed_both = []
    for project in projects:
        cache[project] = True
    for assessment in assessments:
        cache[assessment] = False
    for student in cache:
        if cache.get(student):
            passed_both.append(student)
    return passed_both


print(who_finished_project_and_passed_assessment(finished_project, failed_assessment))

# OUTPUT ==>  ['Stan', 'Craig', 'Tweek', 'Tolken', 'Wendy', 'Jimmy']


# Create a fxn named “double_index” with two parameters: a list and an index number. The fxn should double the value of the list element at the specified index and return the list with the doubled value.
# If the index is not a valid index, the fxn should return the original list.

# Input: ([3,8,-10,12], 2) => Output: [3,8,-20,12]
# Input: ([3,8,-10,12], 6) => Output: [3,8,-10,12]


def double_index(list, index):
    if index < 0 or index > len(list):
        return list

    list[index] = list[index] * 2
    return list


list_nums = [3, 8, -10, 12]
index_num = 2
print(double_index(list_nums, index_num))


# Create a fxn named “remove_middle” which has three parameters: a list, a start number, an end number. The fxn should return a sub-list of the list containing all of the elements between the start and end indexes.
def remove_middle(list, start, end):
    end = end + 1
    return list[start:end]


print(remove_middle([4, 8, 15, 16, 23, 42], 1, 3))


# Create a fxn named “more_than_n” that has three parameters: a list, an item, and a number. The fxn should return “True” if the item appears more then the number of times specified. Otherwise, the fxn should return “False”.
def more_than_n(list, item, num):
    count = list.count(item)
    if count > num:
        return True
    else:
        return False


print(more_than_n([2, 4, 6, 2, 3, 2, 1, 2], 2, 3))


# Create a fxn named “most_frequent_item” that has three parameters: a list, a first item, a second item. Return either the first item or the second item depending on which occurs more often in the list. If the two items appear the same number of times, return the first item.
def most_frequent_item(list, first, second):
    count1 = list.count(first)
    count2 = list.count(second)

    if count2 > count1:
        return second
    else:
        return first


print(most_frequent_item([2, 3, 3, 2, 3, 2, 3, 2, 3], 2, 3))


# Create a fxn named “middle_element” that has one parameter: a list. If there are an odd number of elements in the list, return the middle element. If there are an even number, return the average of the middle elements.


def middle_element(list):
    middle = int(len(list) / 2)
    if len(list) % 2 != 0:
        return list[middle]
    else:
        avg = (list[middle] + list[middle - 1]) / 2
        return avg


print(middle_element([5, 2, -10, -4, 4, 5]))
print(
    middle_element(
        [
            5,
            2,
            -10,
            -4,
            4,
        ]
    )
)


# Create a fxn named “append_sum” that has one parameter: a list. The fxn should add the last two elements of the list together and add the result to the end of the list. It should perform this process three times and then return the list.


def append_sum(list):
    for i in range(3):
        sum = list[-1] + list[-2]
        list.append(sum)
    return list


print(append_sum([1, 1, 2]))


# Create a fxn named “combine_sort” that has two parameters: two lists. The fxn should combine the two lists into a new list, sort the result, and then return the new list.


def combine_sort(list11, list19):
    list11.extend(list19)
    list11.sort()
    return list11


print(combine_sort([4, 10, 2, 5], [-10, 2, 5, 10]))


# Create a fxn named “append_size” that has one parameter: a list. The fxn should add all of the numbers between 1 and the size of the list to the end of the list, and then return the list.
def append_size(list14):
    len_list = len(list14) + 1
    for nums in range(1, len_list):
        list14.append(nums)

    return list14


print(append_size([23, 42, 108]))


# Create a fxn named “every_three_nums” that has one parameter: a number. The fxn should return a list of every third number between the number passed in and 100 (inclusive). If the number passed in is greater then 100, it should return and empty list.
def every_three_nums(number):
    third_list = []
    if number > 100:
        return []
    for x in range(number, 101, 3):
        third_list.append(x)

    return third_list


print(every_three_nums(91))
print(every_three_nums(191))


# Create a fxn named “greetings” that has one parameter: a list of names. In the fxn add the string “Hello” in front of each name and return the list.
def greetings(names_list):
    new_list = []
    for name in names_list:
        new_name = "Hello " + name
        new_list.append(new_name)
    return new_list


print(greetings(["Sam", "Jim", "Sophie"]))


# Create a fxn called “max_num” that has one parameter: a list of numbers. The fxn should return the largest number in the list
def max_num(number_list):
    return max(number_list)


print(max_num([-10, 0, 20, 50, 75, 15]))


# Create a fxn “same_values” that has two parameters: 2 lists of equal size. The fxn should return a list of indexes where there are equal values in each list.
def same_values(lista, listb):
    lengtha = len(lista)
    lengthb = len(listb)
    if lengtha >= lengthb:
        return [index for index, elem in enumerate(listb) if elem == lista[index]]

    return [index for index, elem in enumerate(lista) if elem == listb[index]]


print(same_values([5, 1, -10, 3, 3], [5, 10, -10, 3, 5]))


# Create a fxn named “reversed_list” that has 2 parameters: 2 lists. If the first list is the same as the second list reversed, the fxn should return “True”. If not, it should return “False”.
def reversed_list(listc, listd):
    listd.reverse()
    for item in listc:
        if listc[item] == listd[item]:
            return True

    return False


print(reversed_list([1, 2, 3], [3, 2, 1]))


# Create a fxn named “make_spoonerism” that has 2 parameters: 2 strings. The fxn should return the two words with the first letters switched.
def make_spoonerism(stringa, stringb):
    a = stringa[0:1]
    b = stringb[0:1]
    stringa = b + stringa[1:]
    stringb = a + stringb[1:]
    return f"{stringa} {stringb}"


print(make_spoonerism("Hello", "John"))


# Create a fxn named “sum_values” that takes one parameter: a dictionary. The fxn should return the sum of the values in the dictionary.
def sum_values(my_numdict):
    summ = 0
    for numero in my_numdict.values():
        summ += int(numero)
    return summ


print(sum_values({"milk": 5, "eggs": 2, "flour": 3}))


# Create a fxn named “sum_even_keys” that takes in one parameter: a dictionary. The fxn should return the sum of all of the even key’s values.
def sum_even_keys(key_dict):
    sumz = 0
    for key in key_dict.keys():
        if key % 2 == 0:
            sumz += key
    return sumz


print(sum_even_keys({1: 5, 2: 2, 3: 3}))


# Create a fxn named “add_ten” that takes in one parameter: a dictionary. The
# fxn should add 10 to every value in the dictionary and then return it.
def add_ten(dict_10):
    for key, value in dict_10.items():
        value += 10
        dict_10[key] = value

    return dict_10


print(add_ten({1: 5, 2: 2, 3: 3}))


# Reverse a list

list4 = [100, 200, 300, 400, 500]
list4.reverse()
print(list4)

# Concatenate two lists by index
list1 = ["M", "na", " ", " "]
list2 = ["y", "me", "is", "Anaka"]
list3 = []

for i in range(len(list1)):
    list3.append(list1[i]+list2[i])
result = str(list3)

print(result)

# Turn every item of a list into its square
numbers = [1, 2, 3, 4, 5, 6, 7]

for num in numbers:
    num = num**2
    print(num)


# Given two lists, iterate both lists simultaneously and display items from
# list1 in the original order and items from list2 in reverse order.
list5 = [10, 20, 30, 40]
list6 = [100, 200, 300, 400]
list6.reverse()

list7 = [list(i) for i in zip(list5, list6)]
print(list7)

# Remove empty strings from the list
list8 = ["Mike", "", "Emma", "Aisha", "", "Anaka"]
filtered_list = [i for i in list8 if i != ""]
print(filtered_list)

# OR
def check_list(name):
    if name != "":
        return True


filtered_list = filter(check_list, list8)
result = list(filtered_list)
print(result)

# Add new item to a list after a specified item
# Task: Add item 7000 after 6000 in the following list
list9 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
list9[2][2].append(7000)
print(list9)


# Extend nested list by adding the sub-list
# Expected output ['a', 'b', ['c', ['d', 'e', ['f', 'g', 'h', 'i', 'j'],
#  'k'], 'l'], 'm', 'n']
list10 = ["a", "b", ["c", ["d", "e", ["f", "g"], "k"], "l"], "m", "n"]
sub_list = ["h", "i", "j"]
# print(list10[2]) print to find the index if can't figure out
list10[2][1][2].extend(sub_list)
print(list10)

shipment = [
{"name": "product1", "weight": 100, "qty": 5}
]
def find_weight(shipment):
    weight = 0
    for item in shipment:
        weight += item["weight"] * item["qty"]
    return weight

def longest_string(strings):
    longest = ""
    for string in strings:
        if len(string) > len(longest):
            longest = string
    return longest

def pair_up(items):
    pairs = []
    for i in range(0, len(items), 2):
        pairs.append([items[i], items[i+1]])
    return pairs

#split a string into a list of strings
def join_strings(strings, separator):
    result = ""
    for string in strings:
        result += string + separator
    return result[:-len(separator)]

# Given a phone number as a ten digit number (1234567890, for example), complete the function scammer to return the message ignore if the number satisfies all four of the following properties:

# The first digit is a 2 or 4
# The fifth digit is a 2 or 4
# The seventh and eight digits are the same
# The last digit is a 0
# If the phone number does not satisfy all four of those properties, then return accept.
def scammer(number):
    num_str = str(number)

    if num_str[0] == "2" or num_str[0] == "4":
        if num_str[4] == "2" or num_str[4] == "4":
            if num_str[6] == num_str[7]:
                if num_str[-1] == "0":
                    return "ignore"
    else:
        return "accept"

print(scammer(1234567890))
print(scammer(2222222220))


def num_same_spaces(yesterday, today):
    count = 0
    for i in range(len(yesterday)):
        if yesterday[i] == "C" and today[i] == "C":
            count += 1
    return count

# cipher
def cipher(message, shift):
    new_mess = ''

    for char in message:
        value = ord(char)
        new_value = value + shift
        letter = chr(new_value)

        new_message += letter

    return new_message

# 3 Card Monty

def monty(swaps):
    queen = "middle"

    for swap in swaps:
        if swap == "L":
            if queen == "left":
                queen = "middle"
            else:
                queen = "left"
        elif swap == "R":
            if queen == "right":
                queen = "middle"
            else:
                queen = "right"
        elif swap == "O":
            if queen == "right":
                queen == "left"
            elif queen == "left":
                queen = "right"
    return queen

# Find matches for 2 strings
def num_same_spaces(yesterday, today):
    count = 0
    for i in range(len(yesterday)):
        if yesterday[i] == "C" and today[i] == "C":
            count += 1
    return count

# Allowance and balance

def cash_on_hand(expenses):
    # debit the account each iteration
    # add balance back in each iter
    allowance = 30
    balance = 30
    for expense in expenses:
        balance -= expense
        balance += allowance
    return balance


# Strong password validator
def valid_password(password):
    low_count = 0
    up_count = 0
    num_count = 0

    if len(password) >= 8 and len(password) <= 12:
        for char in password:
            if char.isalnum():
                if char.islower():
                    low_count += 1
                elif char.isupper():
                    up_count += 1
                else:
                    num_count += 1
        if low_count >= 2 and up_count >= 3 and num_count >= 2:
            return "good"
    return "bad"

# compare 2 strings by character
def grade_scantron(submission, answer_key):
    count = 0
    for i in range(len(answer_key)):
        if submission[i] == answer_key[i]:
            count += 1
    return count

# Count the number of times character chunk appears in a string
def language(text):
    count1 = 0
    count2 = 0

    for i in range(len(text)):
        if text[i] == "i" and text[i + 1] == "e":
            count1 += 1
        elif text[i] == "e" and text[i + 1] == "i":
            count2 += 1

    if count1 > count2:
        return "English"
    elif count1 < count2:
        return "German"
    else:
        return "Maybe French?"

# Item swap in list
def calculate_playlist(button_pushes):
    button_list = ["A", "B", "C", "D", "E"]

    for i in range(len(button_pushes)):

        if button_pushes[i] == 1:
            button_list[0], button_list[1] = button_list[1], button_list[0]
        elif button_pushes[i] == 2:
            button_list.append(button_list.pop(0))
        elif button_pushes[i] == 3:
            button_list.insert(0, button_list.pop())

    return button_list

# Hakf-time score using lists
def half_time_scores(
    game_length,  # in minutes
    team_1_score_times,  # each value is seconds
    team_2_score_times   # each value is seconds

):
    points = 0
    game_length = game_length * 60
    half_time = game_length / 2

    for score_time in team_1_score_times:
        if score_time < half_time:
            points += 1
    for score_time in team_2_score_times:
        if score_time < half_time:
            points += 1
    return points

#Given a list of numbers numbers, complete the function mode that returns a list
# of all of the modes of the list of numbers.
def mode(numbers):
    mode_list = []
    count = {}
    for num in numbers:
        if num not in count:
            count[num] = 1
        else:
            count[num] += 1
    for key in count.keys():
        if count[key] == max(count.values()):
            mode_list.append(key)
    return mode_list


# Compare 2 lists
def find_missing_registrant(registrants, finishers):
    for runner in registrants:
        if runner not in finishers:
            return runner

# Invoice past due

from datetime import datetime
class Invoice:
    def __init__(self, customer_name, amount, invoice_date):
        self.customer_name = customer_name
        self.amount_due = amount
        self.invoice_date = invoice_date

def amount_due(invoices, days=30):
    past_due = 0
    for invoice in invoices:
        if (datetime.now() - invoice.invoice_date).days > days:
            past_due += invoice.amount_due
    return past_due

invoices = [
    Invoice("Raul", 25.00, datetime(2010, 4, 15)),
    Invoice("Poli", 50.00, datetime(2029, 11, 5)),
    Invoice("Don", 75.00, datetime(2012, 7, 21)),
    Invoice("Anne", 100.00, datetime(2035, 6, 17))
]

result = amount_due(invoices, 90)
print(result)

# Count matches in list of dictionaries

def count_matches(items, param2="green"):
    matches = []
    for item in items:
        if(item.get("color") == param2):
            matches.append(item)
    return len(matches)

input = [
    {"background": "green",  "size": 5,  "color": "blue"},
    {"background": "yellow", "size": 5,  "color": "green"},
    {"background": "blue",   "size": 25, "color": "green"},
    {"background": "yellow", "size": 5,  "weight": "light"},
]

result = count_matches(input)
print(result)

#Delimiters and split method, split method splits a string and returne a list
def read_delimited(line, separator=","):
    values = line.split(separator)
    return values

input = "1,2,3,4"
read_delimited(input)  # --> ["1","2","3","4"]

# Find shortest list in list of lists
def find_shortest(lists):
    shortest = []

    if len(lists) < 1:
        return None
    elif len(lists) == 1:
        return lists
    else:
        for i in range(len(lists)):
            shortest = lists[i]
            if len(lists[i - 1]) < len(lists[i]):
                shortest = lists[i - 1]
        return shortest


# compare values in a dictionary
company = [
    {
        "name": "Leah",
        "position": "Manager",
        "rate": 45,
        "hours": 36
    },
{
        "name": "Anaka",
        "position": "Software Engineer",
        "rate": 65,
        "hours": 32
    },
    {
        "name": "Xti",
        "position": "Owner",
        "rate": 120,
        "hours": 16
    },
    {
        "name": "Rigby",
        "position": "HR",
        "rate": 25,
        "hours": 40
    },
]

def who_made_most(company):
    made_most = None
    most_money = 0

    for person in company:
        gross_pay = person["rate"] * person["hours"]
        if (not made_most and not most_money) or gross_pay > most_money:
            made_most = person["name"]
            most_money = gross_pay
    return made_most

print(who_made_most(company))

thisdict = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(thisdict["brand"])

thisdict = {
  "brand": "Ford",
  "electric": False,
  "year": 1964,
  "colors": ["red", "white", "blue"]
}

print(thisdict["colors"][2])  # blue


# Create a fxn named “values_are_keys” that takes in one parameter: a dictionary.
# The fxn should return a list of values that also equal keys.
def values_are_keys(same_dict):
    same_list = []
    for key, value in same_dict.items():
        if (key) == value:
            same_list.append(key)
    return same_list

print(values_are_keys({1: 100, 2: 2, 3: 4, 4: 4}))


# Create a fxn named “max_key” that takes in one parameter: a dictionary.
# The fxn should return the key associated with the largest value.
def max_key(max_dict):
    return max(max_dict.keys())

print(max_key({'a': 100, 'b': 10, 'c': 1000}))


# Create a fxn named ”word_length_dict” that takes in one parameter:
# a list of strings. The fxn should return a dictionary where each key
# is a word in the word list and the value is the length of that word.
def word_length_dict(str_list):
    str_dict = {}
    for thingy in str_list:
        str_len = len(thingy)
        str_dict[thingy] = str_len
    return str_dict

print(word_length_dict(['apple', 'cat', 'pickle']))


# Create a fxn named “frequency_count” that takes in one parameter: a list.
# The fxn should return a dictionary containing the frequency of each element
# in the list.
def frequency_count(listr):
    listr_dict = {}
    for item in listr:
        if item not in listr_dict:
            listr_dict[item] = 1
        else:
            listr_dict[item] += 1
    return listr_dict

print(frequency_count([0, 0, 0, 0, 0]))


# Create a fxn named “unique_values” that takes in one parameter:
# a dictionary. The fxn should return the number of unique values
# in the dictionary.
def unique_values(uniq_dict):
    return len(set(uniq_dict))

print(unique_values({0: 3, 1: 6, 4: 9, 5: 1}))


# TODO Create a fxn named “count_first_letter” that takes in one parameter:
# a dictionary with a last name as a key and a list of first names as the
# value. The fxn should return a dictionary where each key is the first
# letter of the last name and the value is the number of people whose last
# name begins with that letter.
def count_first_letter(name_dict):





# TODO Create a fxn named “get_change” that gets user input for a cash total.
# Calculate the total number of Quarters / Dimes / Nickels / Pennies for
# the change exchange. The fxn should return a print of those calculations.
# Note: make this a “hungry” fxn that gets the largest number of large coins
# and smallest number of small coins.
# Note: validate the input to ensure proper input from the user, cover all
# edge case entries, do not allow breaking errors.


def partition(list, size):
    chunks = []
    in_progress = []
    for item in list:
        if len(in_progress) == size:
            chunks.append(in_progress)
            in_progress = []
        in_progress.append(item)
    return chunks


from datetime import datetime
class Invoice:
    def __init__(self, customer_name, amount, invoice_date):
        self.customer_name = customer_name
        self.amount_due = amount
        self.invoice_date = invoice_date

def amount_due(invoices, days=30):
    past_due = 0
    for invoice in invoices:
        if (datetime.now() - invoice.invoice_date).days > days:
            past_due += invoice.amount_due
    return past_due

def c_to_f(degrees_celsius):
    return degrees_celsius / (5/9) + 32

def check_age(name, age):
    if age < 21:
        return (f"Go home, {name}!")
    else:
        return (f"Welcome, {name}!")


def is_key_in(dictionary, key):
    for k in dictionary.keys():
        if k == key:
            return True
        return False


def make_description(name, attributes):
    description = name
    for key, value in attributes.items():
        description = description + ", " + value + " " + key
    return description


def reverse_entries(dictionary):
    return {value: key
            for key, value
            in dictionary.items()}


class Car:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year

    def __str__(self):
        return f'{self.year} {self.make} {self.model}'


def horizontal_bar_chart(sentence):
    dict = {}
    for char in sentence:
        if char >= "a" and char <= "z":
            if char not in dict:
                dict[char] = ""
            dict[char] += char
    res = []

    for value in dict.values():
        res.append(value)

    return list(sorted(res))


def find_shortest(lists):
    shortest = []

    if len(lists) < 1:
        return None
    elif len(lists) == 1:
        return lists
    else:
        for i in range(len(lists)):
            shortest = lists[i]
            if len(lists[i - 1]) < len(lists[i]):
                shortest = lists[i - 1]
        return shortest

def calculate_monthly_savings(salary, save_rate):
    month_salary = salary / 12
    savings = month_salary * save_rate

    return savings


def total_revenue(product_sales):
    total = 0
    sub_total = 0

    # iterate through dictionary to get values
    # multiply values
    for i in range(len(product_sales)):
        sale = product_sales[i]['sales']
        cost = product_sales[i]['price']
        sub_total = sale * cost
        total += sub_total
    return total

# There's a "3 for 2" offer on mangoes. For a given quantity and price (per 3 mangoes), calculate the total cost of the mangoes, plus tax. Mangoes can only be purchased in quantities of 3.


# Implement the checksum method using the following algorithm:

# Start with a sum of zero
# For each letter in the message:
# Convert the letter to a number using the ord function
# Add the number to the sum
# Use modulo division % to find the remainder when the sum is divided by 26
# Add 97 to the remainder
# Convert the sum from the previous step to a letter using the chr function
# Return that letter

def checksum(message):
    sum = 0
    for letter in message:
        sum += ord(letter)
    return chr(sum % 26 + 97)
